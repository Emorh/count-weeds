import os
import torch
import cv2

import albumentations              as A
import matplotlib.pylab            as plt
import numpy                       as np
import pytorch_lightning           as pl
import segmentation_models_pytorch as smp

from PIL              import Image
from pprint           import pprint
from torch._C         import device
from torch.utils.data import DataLoader
from torch.utils.data import Dataset

from tqdm import tqdm

plt.style.use('default')


def pyplot(func):
    def make_pyplot_context(*args, **kwargs):
        plt.figure(figsize=kwargs.get('size', (10, 5)))
        ax = plt.axes([0,0,1,1], frameon=False)
        ax.set_axis_off()
        return func(*args, **kwargs)
    return make_pyplot_context

@pyplot
def show_bgr(img, size=(10, 5)):
    plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))

@pyplot
def show(img, size=(10, 5)):
    plt.imshow(img)

@pyplot
def show_gray(img, size=(10, 5)):
    plt.imshow(img, cmap='gray')

def mse(img1, img2):
    return ((img1.astype(int) - img2.astype(int)) ** 2).mean()

def get_img(img):
  return img.to(torch.uint8).permute(1, 2, 0)

def get_mask(mask):
  m = torch.zeros(mask.shape[1:])
  for i in range(mask.shape[0]):
    m += mask[i] * (i + 1)
  return m


class Dataset(Dataset):
    def __init__(self, dir_to_img, transform=None):
        self.image_dir = dir_to_img + '/images'
        self.mask_dir = dir_to_img + '/masks'
        self.transform = transform
        self.images = os.listdir(self.image_dir)

    def __len__(self):
        return len(self.images)

    def __getitem__(self, index):
        img_path = os.path.join(self.image_dir, self.images[index])
        mask_path = os.path.join(self.mask_dir, self.images[index])

        image = cv2.imread(img_path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        mask = cv2.imread(mask_path, 0)
        masks = [(mask == v) for v in np.unique(mask)]        
        mask = np.stack(masks, axis=-1).astype('float')

        if self.transform is not None:
            augmentations = self.transform(image=image, mask=mask)
            image = augmentations['image']
            mask = augmentations['mask']

        image = torch.tensor(image).permute(2, 0, 1).to(torch.float32)
        mask  = torch.tensor(mask).permute(2, 0, 1)

        return image, mask

HEIGHT_IMAGE=1728
WIDTH_IMAGE =2592

TRAIN_PATH="train"
VAL_PATH  ="val"
TEST_PATH ="test"

BATCH_SIZE_TRAIN=1
BATCH_SIZE_VAL  =1
BATCH_SIZE_TEST =1

ENCODER         ='resnet18'
ENCODER_WEIGHTS ='imagenet'
CLASSES         =7
ACTIVATION      ='softmax2d' # could be None for logits or 'softmax2d' for multiclass segmentation
DEVICE          ='cuda'

LEARNING_RATE        =0.0001
EPOCHS               =30
PATH_TO_MODEL_WEIGHTS='./best_model.pth'

TRAIN_RES_FOLDER="TrainRes"
VAL_RES_FOLDER  ="ValRes"
TEST_RES_FOLDER ="TestRes"

[os.mkdir(f) for f in [TRAIN_RES_FOLDER, VAL_RES_FOLDER, TEST_RES_FOLDER] if not os.path.exists(f)]
'''for folder in [TRAIN_RES_FOLDER, VAL_RES_FOLDER, TEST_RES_FOLDER]:
  if not os.path.exists(folder):
    os.mkdir(folder)'''


train_transforms = A.Compose(
    [
        A.Resize(height=HEIGHT_IMAGE, width=WIDTH_IMAGE),
    ])

train_transforms_out = A.Compose(
    [
        A.Resize(height=HEIGHT_IMAGE, width=WIDTH_IMAGE),
    ])

val_transforms = A.Compose(
    [   
        A.Resize(height=HEIGHT_IMAGE, width=WIDTH_IMAGE)
    ])

test_transforms = A.Compose(
    [   
        A.Resize(height=HEIGHT_IMAGE, width=WIDTH_IMAGE)
    ])


train_dataset       = Dataset(TRAIN_PATH, train_transforms)
train_dataset_out   = Dataset(TRAIN_PATH, train_transforms_out)
val_dataset         = Dataset(VAL_PATH, val_transforms)
test_dataset        = Dataset(TEST_PATH, test_transforms)

val_loader    = torch.utils.data.DataLoader(val_dataset, BATCH_SIZE_VAL, pin_memory=False, shuffle=True)
train_loader  = torch.utils.data.DataLoader(train_dataset, BATCH_SIZE_TRAIN, pin_memory=False, shuffle=True)
test_loader   = torch.utils.data.DataLoader(test_dataset, BATCH_SIZE_TEST, pin_memory=False, shuffle=True)


# create segmentation model with pretrained encoder
model = smp.Unet(
    encoder_name=ENCODER, 
    encoder_weights=ENCODER_WEIGHTS, 
    classes=CLASSES, 
    activation=ACTIVATION,
)

loss = smp.utils.losses.DiceLoss()
metrics = [
    smp.utils.metrics.IoU(threshold=0.5),
]

optimizer = torch.optim.Adam([ 
    dict(params=model.parameters(), lr=LEARNING_RATE),
])

train_epoch = smp.utils.train.TrainEpoch(
    model, 
    loss=loss, 
    metrics=metrics, 
    optimizer=optimizer,
    device=DEVICE,
    verbose=True,
)

valid_epoch = smp.utils.train.ValidEpoch(
    model, 
    loss=loss, 
    metrics=metrics, 
    device=DEVICE,
    verbose=True,
)

max_score = 0
train_loss = []
train_metrics = []
val_loss = []
val_metrics = []

for i in range(0, EPOCHS): 
    print('\nEpoch: {}'.format(i))
    train_logs = train_epoch.run(train_loader)
    valid_logs = valid_epoch.run(val_loader)
    
    train_loss.append(train_logs['dice_loss'])
    train_metrics.append(train_logs['iou_score'])
    val_loss.append(valid_logs['dice_loss'])
    val_metrics.append(valid_logs['iou_score'])
  

    '''    
    if i == 25:
        optimizer.param_groups[0]['lr'] = 1e-5
        print('Decrease decoder learning rate to 1e-5!')
    '''

x = [i + 1 for i in range(len(train_loss))]

print("train loss:")
print(train_loss)
print("train metrics:")
print(train_metrics)
print("val loss:")
print(val_loss)
print("val metrics:")
print(val_metrics)

plt.figure()
plt.plot(x, train_loss, label='Train loss')
plt.plot(x, val_loss, label='Val loss')

plt.xlabel("Iteration", fontsize=14, fontweight="bold")
plt.ylabel("Loss", fontsize=14, fontweight="bold")
plt.legend()

plt.savefig('Loss.png')

def get_count_weeds(mask):
    classes = [0, 0, 0, 0, 0]

    classes[0] = mask[2].sum() / (mask[1].sum() / 37)
    classes[1] = mask[3].sum() / (mask[1].sum() / 7237)
    classes[2] = mask[4].sum() / (mask[1].sum() / 5847)
    classes[3] = mask[5].sum() / (mask[1].sum() / 2526)
    classes[4] = mask[6].sum() / (mask[1].sum() / 4025)

    return classes

for index in tqdm(range(len(train_dataset))):
  im, mask = train_dataset_out[index]
  plt.imsave(f'{TRAIN_RES_FOLDER}/RealIm_{index}.png', get_img(im).to('cpu').detach().numpy())
  plt.imsave(f'{TRAIN_RES_FOLDER}/RealMask_{index}.png', get_mask(mask).to('cpu').detach().numpy())
  im = torch.unsqueeze(im, dim=0)
  im = im.to(DEVICE)
  new_mask = model(im)[0].to('cpu').detach().numpy()
  plt.imsave(f'{TRAIN_RES_FOLDER}/ModelMask_{index}.png', get_mask(new_mask))

print("Start output val")
for index in tqdm(range(len(val_dataset))):
  im, mask = val_dataset[index]
  plt.imsave(f'{VAL_RES_FOLDER}/RealIm_{index}.png', get_img(im).to('cpu').detach().numpy())
  plt.imsave(f'{VAL_RES_FOLDER}/RealMask_{index}.png', get_mask(mask).to('cpu').detach().numpy())
  im = torch.unsqueeze(im, dim=0)
  im = im.to(DEVICE)
  new_mask = model(im)[0].to('cpu').detach().numpy()
  counts = np.around(get_count_weeds(new_mask))
  print(f"val {index}")
  print(counts)
  plt.imsave(f'{VAL_RES_FOLDER}/{index}.png', get_mask(new_mask))

print("Start output test")
for index in tqdm(range(len(test_dataset))):
  im, mask = test_dataset[index]
  plt.imsave(f'{TEST_RES_FOLDER}/RealIm_{index}.png', get_img(im).to('cpu').detach().numpy())
  plt.imsave(f'{TEST_RES_FOLDER}/RealMask_{index}.png', get_mask(mask).to('cpu').detach().numpy())
  im = torch.unsqueeze(im, dim=0)
  im = im.to(DEVICE)
  new_mask = model(im)[0].to('cpu').detach().numpy()
  counts = np.around(get_count_weeds(new_mask))
  print(f"tests {index}")
  print(counts)
  plt.imsave(f'{TEST_RES_FOLDER}/{index}.png', get_mask(new_mask))


print('Test metrics')
test_logs = train_epoch.run(test_loader)

print(f"Test dice loss = {test_logs['dice_loss']}")
print(f"Test IoU metrics = {test_logs['iou_score']}")
